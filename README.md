# 1.0  Adventeurs PWA
** **THIS PAGE IS A WORK IN PROGRESS. NOTHING IS IN A FINALIZED STATE AND SOME INFORMATION WILL BE INCOMPLETE** ** 

**1.1** Adventeurs is the one place on the internet for adventure seekers and nature lovers to share their experiences and be rewarded for doing so. The aim of the app is provide and build a community for creators of all mediums. It will be the first app to feature creators of all disciplines in one place. By editorialising the content, we can avoid over saturation of content and enable a more evenly distributed exposure to subscribers, this in turn increases the amount of engagement for each creator. Subscribers will be able to engage with content by giving a *seed*. Every subscription comes with a monthly allotment of seeds, and the price of the subscription is determined by how many seeds a user wants to give a month. One seed is close to one dollar in value to a creator. A shop will feature adventure / nature themed merchandise from small brands and individuals as well as a section dedicated purely to self published authors.
&emsp;:fire::green_book::fire: **Repo** --> [AdventeurMVP](https://gitlab.com/justinmillerdev/AdventeurMVP)

**1.2** Getting started
[ClickUp](https://app.clickup.com/10521219/v/b/t/10521219) 

**1.3** Table of Contents
&emsp;&emsp;[Design](#design)
&emsp;&emsp;[Data](#data)
&emsp;&emsp;[Deployment](#deployment) 
&emsp;&emsp;[Microservices](#microservices)
&emsp;&emsp;[API's](#apis)
&emsp;&emsp;[Google Analytics](#google-analytics)
&emsp;&emsp;[Dashboard](#dashboard)
&emsp;&emsp;[Features](#features)



## 2.0 Design

**2.1** :pencil2: [Style Guide & Mood Board](https://www.figma.com/file/8yKW73jsl316ypN3wL3Bz9/Style-Guide-and-Mood-Board?node-id=0%3A1)
<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2F8yKW73jsl316ypN3wL3Bz9%2FStyle-Guide-and-Mood-Board%3Fnode-id%3D0%253A1" allowfullscreen></iframe>


**2.2** :pencil2: [Adventeurs Web Applicaiton ](https://www.figma.com/file/H51cbniBRBlI8MNVYnEztV/Adventeurs-%28?node-id=468:987)
<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FH51cbniBRBlI8MNVYnEztV%2FAdventeurs%3Fnode-id%3D0%253A1" allowfullscreen></iframe>


 **2.3** [Progessive Web App (Mobile)](https://www.figma.com/file/4cVEUvevhWLvdHeeTNAlGh/Adventeurs-Mobile?node-id=0:1)
 <iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2F4cVEUvevhWLvdHeeTNAlGh%2FAdventeurs-Mobile%3Fnode-id%3D0%253A1" allowfullscreen></iframe>

## 3.0 Data

**3.1** :pencil2: [**Data Diagram for PWA**](https://www.figma.com/file/G1LSYylhQPJqG9rGf5vxW4/Adventeurs-Data-Diagram?node-id=3:728) 
&emsp; Where green signifies custom data types whose fields are nested within the document. Data structures and relationships with Foreign Keys (FK) and Primary Keys (PK). NoSQL data model includes data duplication among documents . This reduces the amount of reads required to retrieve certain data, such as the creator info for displaying within posts and post previews. This also requires multiple writes, to update the data when it is changed.
<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FG1LSYylhQPJqG9rGf5vxW4%2FAdventeurs-Data-Diagram%3Fnode-id%3D3%253A728" 
allowfullscreen></iframe>

**3.2**  :pencil2: [Data Diagram for Submission and Editorial](https://www.figma.com/file/DlDwnFDkVq5vELF3fwcRRT/Editorial-Data-Diagram?node-id=3:728)
&emsp; Data undergoes a series of transformations before it is normalised into data to be used in the application. Contributors will make their submission on a dynamic form which will then be normalised into a data structure for view and accepting submissions. Data that is accepted for publication will undergo another process of normalisation to add metadata and other SEO oriented data. Upon acceptance posts will be left in a queue for next monthly "Edition". Publication dates will marked down to the millisecond for pagination purposes initially. Once a recommendation algorithm has been established a separate field will be used for pagination and display of content.
<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FDlDwnFDkVq5vELF3fwcRRT%2FEditorial-Data-Diagram%3Fnode-id%3D3%253A728" allowfullscreen></iframe>
 

## 4.0 Deployment

**4.1** Gitlab workflow
 - Feature Branch --> 
	 - All work on features will be completed in a feature branch. Once work on feature is complete a PR to dev can be raised and approval requested.
 - Dev Branch --> 
	 - The Dev branch is the staging area for all new features. When new features are ready for deployment and testing, a PR for test env can be made and approval requested.
 - ⚒️ Test Branch -->
	 -  Code merged to the Test env will be deployed automatically to [URL]. Here, manual testing of the new features can be performed and documented ( see testing guidelines ). Once end to end testing is complete a PR to the main branch can be raised and approval requested.
 - 🚀 Main Branch -->
	 - This is the production version of the build and the final stage for all code changes. Production merges are performed on Mondays at midnight CT to minimize impact. Manual testing of the new features will immediately follow deployment and changes rolled back if a breaking change is found.

**4.2** Applications & Deployment Details

 &emsp;**4.2.1** PWA

 - :green_book: Repo --> &emsp; [AdventeurMVP](https://gitlab.com/justinmillerdev/AdventeurMVP)
 - Deployed to --> &emsp; Firebase Hosting
 - ⚒️ Test domain --> &emsp; https://adventeurs-57632.web.app
 - 🚀 Production domain --> &emsp; https://adventeurs.com
 - Instructions --> &emsp; deployment process is automated through GitLab CI/CD pipeline.

&emsp;**4.2.2** Dashboard

 - :green_book: Repo --> &emsp; [Dashboard](https://gitlab.com/justinmillerdev/dashboard)
 - Deployed to --> &emsp;  Local
 - ⚒️ Test domain --> &emsp; https://localhost:4201
 - 🚀 Production domain --> &emsp; https://localhost:4201
 - Instructions --> &emsp; pull down repo for desired env, populate the desired env file and the command `npm start` in the terminal.

&emsp; **4.2.3** Firebase Functions
 - :green_book: Repo --> &emsp;[Firebase-Functions](https://gitlab.com/justinmillerdev/firebase-functions)
 - Deployed to --> &emsp;[Firebase Functions](https://console.firebase.google.com/u/1/project/adventeurs-57632/functions) 
 - ⚒️ Test domain --> &emsp; n/a
 - 🚀 Production domain --> &emsp;n/a
 - Instructions --> &emsp; deployment process is automated through GitLab CI/CD pipeline.
 
&emsp; **4.2.4** API Gateway
 - :green_book: Repo --> &emsp; [API-Gateway](https://gitlab.com/justinmillerdev/api-gateway)
 - Deployed to --> &emsp;This repo may be migrated to firebase functions. It will serve as a local, easily tested gateway which is then translated to a firebase function. This app may also be deployed separately to GCP.
 - ⚒️ Test domain -->&emsp; TBD
 - 🚀 Production domain -->&emsp; TDB
 - Instructions -->&emsp; pull down the repo and run with `npm start`. App runs on localhost:3000.
 
## 5.0 Microservices



## 6.0 API's

	

**6.1** *[ImageMagick](https://imagekit.io/dashboard)* 
&emsp;URL endpoint https://ik.imagekit.io/wgh2afoeshio/ 
&emsp;image bucket ADVENTEURS 57632 IMAGE BUCKET [Firebase Storage](https://console.firebase.google.com/u/1/project/adventeurs-57632/storage/adventeurs-57632.appspot.com/files/~2F)

**6.2** *[Vimeo](https://developer.vimeo.com/api/reference)*

**6.3** *[Stripe](https://stripe.com/docs/payments/elements)*

**6.4** *[Google Analytics](https://developers.google.com/analytics/devguides/reporting/core/v4)*


## 7.0 Google Analytics



## 8.0 Dashboard

## 9.0 Features

**9.01** Landing Page

 - Call to action

**9.02** Contributor Submission 

**9.03** User Onboarding

 - Sign-in
 - Login
 - Creator Onboarding
 - Subscription Tier

**9.04** Seeds ( Supporting Creators )

 - Giving a seed
 - Receiving a seed

**9.05** Blog
 - Category Page
 - Single Page

**9.06** Video
 - Category Page
 - Single Page

**9.07** Podcast
 - Category Page
 - Single Page

**9.08** Shop
 - Category Page
 - Single Page

**9.09** Written 
 - Category Page
 - Single Page

**9.10** Photography
 - Category Page
 - Single Page

**9.11** Ads ( Free & No-Account Users Only )

**9.12** Offline Persistence 

 - Selecting items for persistence
 - Removing items from device
 - Viewing items that have been downloaded

**9.13** Creator Payout


**9.14** Creator Dashboard

