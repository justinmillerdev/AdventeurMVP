import { NgZone } from '@angular/core';

import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

import { environment } from '../environments/environment';

export function AngularFirestoreDashboard(platformId: Object, zone: NgZone) {
  return new AngularFirestore(environment.firebaseDashboardConfig, 'adventeurs-dashboard', false, null, platformId, zone, null, null, null);
}

export function AngularFirestoreDb(platformId: Object, zone: NgZone) {
  return new AngularFirestore(environment.firebaseConfig, 'firebase-project2', false, null, platformId, zone, null, null, null);
}

export function AngularFireAuthDb(platformId: Object, zone: NgZone) {
  return new AngularFireAuth(environment.firebaseConfig, 'firebase-project2-auth', platformId, zone, null, null, null, null, null, "local");
}