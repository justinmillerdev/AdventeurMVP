import { BrowserModule } from "@angular/platform-browser";
import { NgModule, PLATFORM_ID, NgZone } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { RouterModule } from '@angular/router';
import { AngularFirestoreDashboard, AngularFirestoreDb, AngularFireAuthDb } from './angularfirestore-extension.service';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule.withServerTransition({ appId: 'serverApp' }), AppRoutingModule, RouterModule, ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })],
  providers: [
    {
      provide: 'firestoreDashboard',
      deps: [PLATFORM_ID, NgZone],
      useFactory: AngularFirestoreDashboard
    },
    {
      provide: 'firestore',
      deps: [PLATFORM_ID, NgZone],
      useFactory: AngularFirestoreDb
    },
    {
      provide: 'firestore',
      deps: [PLATFORM_ID, NgZone],
      useFactory: AngularFireAuthDb
    }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
