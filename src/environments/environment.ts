// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBwNSX53Ymq4kNndg2a3nuNrNxL10jjchI",
    authDomain: "adventeurs-57632-16228.firebaseapp.com",
    projectId: "adventeurs-57632-16228",
    storageBucket: "adventeurs-57632-16228.appspot.com",
    messagingSenderId: "650285360934",
    appId: "1:650285360934:web:ee41be437edf2caa9643e7",
    measurementId: "G-HRY2VET0JX"
  },
  firebaseDashboardConfig: {
    apiKey: "AIzaSyDXO6YzuAHhB6Bf6sPMiFhpALLaHd8ElFE",
    authDomain: "adventeurs-dashboard.firebaseapp.com",
    projectId: "adventeurs-dashboard",
    storageBucket: "adventeurs-dashboard.appspot.com",
    messagingSenderId: "166776502675",
    appId: "1:166776502675:web:a4525e829f244f5df01066",
    measurementId: "G-9TQY6NS20E"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
